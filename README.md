# JOGO ADIVINHE A PALAVRA

    - MAIS DE 500 PALAVRAS;
    - OBJECTIVO, ADIVINHAR UMA PALAVRA ESCOLHIDA ALEATORIAMENTE;
    - COM DIREITO A ESCOLHA DE NÍVEIS E NÚMERO DE TENTATIVAS;
    - UMA APARÊNCIA SIMPLES E BASTANTE PRÁTICA;

![adivinha-palavra-icon](img/adivinhapalavra-ico1.png)

## Demonstrações

![pagina-inicial](img/intro_processando.png)
![primeira-informacao](img/intro_info.png)
![instrucoes](img/instrucoes.png)
![palavras-secretas](img/palavras_secretas.png)
![dados-jogador](img/jogador.png)
![erro-nome-jogador-nao-definido](img/nome_jogador_nao_definido.png)
![jogando](img/jogando.png)
![erro-letra-tentativa-nao-atribuida](img/tentativa_nao_atribuida.png)
![acertou](img/acertou.png)
![errou](img/errou.png)
![fimjogo-tentativas-esgotadas](img/tentativas_esgotadas.png)
![fimjogo-ganhou](img/ganhou.png)
![informacoes-programa](img/sobre.png)

---

&COPY; 2019-2021 [Nurul Carvalho](mailto:nuruldecarvalho@gmail.com) \
&TRADE; [ArtesGC](https://artesgc.home.blog)
