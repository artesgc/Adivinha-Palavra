#*******************************************************************************
# (c) 2019-2021. Nurul GC                                                      *
#                                                                              *
# Jovem Programador                                                            *
# Estudante de Engenharia de Telecomunicaçoes                                  *
# Tecnologia de Informação e de Medicina.                                      *
# Foco Fé Força Paciência                                                      *
# Allah no Comando.                                                            *
#*******************************************************************************

ln -f "./adivinhapalavra.desktop" "$HOME/Área\ de\ Trabalho/"
exec ./adivinhapalavra
